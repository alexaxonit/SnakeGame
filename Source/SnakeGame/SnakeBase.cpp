// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Let.h"
#include "Interactable.h"
#include "PlayerPawnBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnBase = nullptr;
	TickTime = 0.5f;
	ElementSize = 100.f;
	CountElementDefault = 2;
	Score = 0;
	LastElementLocation = FVector(0);
	
	MovementDirection = EMovementDirection::UP;
	LastMovementDirection = EMovementDirection::UP;
	

}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorTickInterval(TickTime);
	AddSnakeElement(CountElementDefault);
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; i++)
	{
		FVector NewElementLocation = LastElementLocation;

	
	if(i > 0){
		if(SnakeElements.Num() > 1)
		{
			auto LastElement = SnakeElements[SnakeElements.Num() - 1];
			auto PenultimateElement = SnakeElements[SnakeElements.Num() - 2];
			
			
			NewElementLocation = FVector(
				LastElement->GetActorLocation().X*2 - PenultimateElement->GetActorLocation().X,
				LastElement->GetActorLocation().Y*2 - PenultimateElement->GetActorLocation().Y,
				LastElement->GetActorLocation().Z);
			
		}
		
		else if(SnakeElements.Num() == 1)
		{
			float OffsetX = 0;
			float OffsetY = 0;
			
			switch (LastMovementDirection)
			{
			case EMovementDirection::UP:
				OffsetX -= ElementSize;
				break;
			case EMovementDirection::DOWN:
				OffsetX += ElementSize;
				break;
			case EMovementDirection::LEFT:
				OffsetY += ElementSize;
				break;
			case EMovementDirection::RIGHT:
				OffsetY -= ElementSize;
				break;
			default:
				break;
			}
			
			const auto HeadElement = SnakeElements[0];
			NewElementLocation = FVector(
			HeadElement->GetActorLocation().X + OffsetX,
			HeadElement->GetActorLocation().Y + OffsetY,
			HeadElement->GetActorLocation().Z);
		}
	}
		
		FTransform NewTransform(NewElementLocation);
		ASnakeElementBase* NewSnakeElement = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElement->SnakeOwner = this;

		
		if (SnakeElements.Add(NewSnakeElement) == 0) {
			NewSnakeElement->SetFirstElementType();
		}  else
		{
			NewSnakeElement->SetBodyElementType();
		}
	}
}

void ASnakeBase::Move()
{
	FVector MovementVector(0);
	switch (MovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X = ElementSize;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X = -ElementSize;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y = -ElementSize;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.Y = ElementSize;
		break;
	}
	LastElementLocation = SnakeElements[SnakeElements.Num()-1]->GetActorLocation();
	SnakeElements[0]->ToggleCollision();
	
	for (int i = SnakeElements.Num() - 1; i > 0; i--) {
		auto CurrentElement = SnakeElements[i];
		auto PreviousElement = SnakeElements[i-1];
		FVector NewElementLocation(PreviousElement->GetActorLocation());
		CurrentElement->SetActorLocation(NewElementLocation);
	}

	SnakeElements[0]->AddActorWorldOffset(MovementVector);
	LastMovementDirection = MovementDirection;

	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if(IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if(InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::SpawnArena(int X, int Y)
{
	for(int i = -((X-X%2)/2); i <= (X-X%2)/2; i++)
	{
		for(int j = -((Y-Y%2)/2); j <= ((Y-Y%2)/2); j++)
		{	if((i == -((X-X%2)/2)) || (i == ((X-X%2)/2)) || (j == -((Y-Y%2)/2)) || (j == ((Y-Y%2)/2)))
			{
			FVector LetLocation(i*ElementSize,j*ElementSize,0);
			FTransform LetTransform(LetLocation);
			GetWorld()->SpawnActor<ALet>(LetClass, LetTransform);
			}
		}
	}
}


