// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	FoodTypeRandom = FMath::RandRange(0,3);
	FoodScore = SpeedMultiplier = 1;

}


// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	switch (FoodTypeRandom)
	{
	case 0:
		SetDefaultType();
		break;
	case 1:
		SetMoreScoreType();
		break;
	case 2:
		SetFreezeType();
		break;
	case 3:
		SetFastType();
		break;
	default:
		break;
	}
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if(bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if(IsValid(Snake))
		{
			Snake->TickTime = Snake->TickTime * SpeedMultiplier;
			Snake->Score += FoodScore;
			Snake->AddSnakeElement(FoodScore);
			this->Destroy();
		}
	}
}

void AFood::SetDefaultType_Implementation()
{
	FoodScore = 1;
}

void AFood::SetMoreScoreType_Implementation()
{
	FoodScore = 3;
}

void AFood::SetFreezeType_Implementation()
{
	SpeedMultiplier = 1.27;
}

void AFood::SetFastType_Implementation()
{
	SpeedMultiplier = 0.72;
}

