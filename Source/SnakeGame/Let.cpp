// Fill out your copyright notice in the Description page of Project Settings.


#include "Let.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"

// Sets default values
ALet::ALet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ALet::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALet::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if(IsValid(Snake))
	{
		for(auto SnakeElem : Snake->SnakeElements)
		{
			SnakeElem->Destroy();
		}
		Snake->Destroy();
	}
}

