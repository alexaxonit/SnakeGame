// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "SnakeBase.h"
#include "Components/InputComponent.h"


// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
	RootComponent = PawnCamera;

	GridX = 19;
	GridY = 33;

}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	SetActorRotation(FRotator(-90, 0, 0));
	
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::VerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HorizontalInput);
	PlayerInputComponent->BindAction("Space", IE_Pressed, this, &APlayerPawnBase::CreateSnake);

}

void APlayerPawnBase::CreateSnake()
{
	if (!IsValid(SnakeActor))
	{
			SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
			SnakeActor->PawnBase = this;
			SnakeActor->SpawnArena(GridX, GridY);
			ChangeZCamera(GridX, GridY);
	}

}

void APlayerPawnBase::VerticalInput(float Input)
{
	if (IsValid(SnakeActor)) 
	{
		if (Input > 0 && SnakeActor->LastMovementDirection != EMovementDirection::DOWN)
		{
			SnakeActor->MovementDirection = EMovementDirection::UP;
		}
		else if (Input < 0 && SnakeActor->LastMovementDirection != EMovementDirection::UP)
		{
			SnakeActor->MovementDirection = EMovementDirection::DOWN;
		}
	}
}

void APlayerPawnBase::HorizontalInput(float Input)
{
	if (IsValid(SnakeActor)) 
	{
		if (Input > 0 && SnakeActor->LastMovementDirection != EMovementDirection::LEFT)
		{
			SnakeActor->MovementDirection = EMovementDirection::RIGHT;
		}
		else if (Input < 0 && SnakeActor->LastMovementDirection != EMovementDirection::RIGHT)
		{
			SnakeActor->MovementDirection = EMovementDirection::LEFT;
		}
	}
}

void APlayerPawnBase::ChangeZCamera(int X, int Y)
{
	float ZCamera = Y>X ? Y * 50 : X * 89;
		FVector NewLoc(0,0,ZCamera);
		PawnCamera->SetRelativeLocation(NewLoc);
}

